package domain;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import java.util.ArrayList;

@XmlRootElement
public class Movie {
	
private int id;
private String title;
private String director;
private String writer;

private List<Actors> actors;
private List<Comments> comments;

public String getWriter() {
	return writer;
}
public void setWriter(String writer) {
	this.writer = writer;
}
public List<Actors> getActors() {
	return actors;
}
public void setActors(List<Actors> actors) {
	this.actors = actors;
}

public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getTitle() {
	return title;
}
public void setTitle(String title) {
	this.title = title;
}
public String getDirector() {
	return director;
}
public void setDirector(String director) {
	this.director = director;
}
public List<Comments> getComments() {
	return comments;
}
public void setComments(List<Comments> comments) {
	this.comments = comments;
}
}
